import pymysql


class DatabaseHandler:
    def __init__(self, host, user, password, database, port=3306):
        self.config = {
            'host': host,
            'port': 3306,
            'user': user,
            'password': password,
            'database': database,
            'charset': 'utf8',
            'cursorclass': pymysql.cursors.DictCursor
        }
        self.connection = pymysql.connect(**self.config)

    def close_connection(self):
        self.connection.close()

    def get_user_bin_data(self, uid: str, column_name: str) -> str:
        try:
            if (isinstance(uid, int)):
                uid = str(uid)

            table_name = f't_player_data_{uid[-1]}'

            with self.connection.cursor() as cursor:
                sql = f"SELECT hex({column_name}) AS bin_data FROM {table_name} WHERE uid = {uid}"

                cursor.execute(sql)

                result = cursor.fetchone()
                return result['bin_data'] if result else "No data found." 
        except Exception as e:
            print(f'Error: {e}')

    def set_user_bin_data(self, uid: str, column_name: str, bin_data: str):
        try:
            if (isinstance(uid, int)):
                uid = str(uid)

            table_name = f't_player_data_{uid[-1]}'

            with self.connection.cursor() as cursor:
                # Assuming bin_data is in the format of a hexadecimal string like '0xABCDEF...'
                if bin_data.startswith('0x'):
                    bin_data = bin_data[2:]  # Remove '0x' prefix before storing in the database
                sql = f"""
                UPDATE {table_name}
                SET {column_name} = UNHEX('{bin_data}')
                WHERE uid = {uid}
                """
                cursor.execute(sql)
                self.connection.commit()
                print(f"Data updated successfully for UID: {uid}")
        except Exception as e:
            print(f'Error: {e}')
            self.connection.rollback()  # Rollback in case of error

    def get_user_block_data(self, uid: str, column_name: str) -> list:
        try:
            if isinstance(uid, int):
                uid = str(uid)

            table_name = f't_block_data_{uid[-1]}'

            with self.connection.cursor() as cursor:
                sql = f"SELECT hex({column_name}) AS bin_data FROM {table_name} WHERE uid = {uid}"
                cursor.execute(sql)

                results = cursor.fetchall()
                return [row['bin_data'] for row in results] if results else ["No data found."]
        except Exception as e:
            print(f'Error: {e}')
            return []