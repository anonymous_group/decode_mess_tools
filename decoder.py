import binascii
import re
import time
import zlib
from dbconnector import DatabaseHandler
from protoc_decode import ProtocDecoder
from protoc_octal_to_utf8_bytes import protoc_octal_to_utf8_bytes

import os
os.chdir(os.path.dirname(os.path.abspath(__file__)))


_OLD_UID_INTS = [000000000]
_NEW_UID_INTS = 000000000


class Decoder:
    dbh = DatabaseHandler('172.16.2.2', 'root', 'Passw0rld', 'hk4e_db_user')

    @staticmethod
    def hex_str2bytes(hex_lines: str) -> bytes:
        hex_data = ''.join(line.strip() for line in hex_lines)
        # TODO: better logic?
        hex_data = hex_data.removeprefix('0x')
        hex_data = hex_data.removeprefix('5A4C4942')
        binary_data = bytes.fromhex(hex_data)
        return binary_data

    @staticmethod
    def bytes2hex_str(binary_data: bytes) -> bytes:
        hex_data = '0x' + binascii.hexlify(binary_data).decode('utf-8').upper()
        return hex_data

    @staticmethod
    def decompress_file(compressed_data: bytes):
        try:
            uncompressed_data = zlib.decompress(compressed_data)
            return uncompressed_data
        except Exception as e:
            print(f'Error: {e}\nDecompress failed, skipping this decompression')
            return compressed_data

    @staticmethod
    def compress_file(data: bytes):
        compressed_data = zlib.compress(data)
        return str('ZLIB').encode('utf-8') + compressed_data  # add 'ZLIB' for special situation.

    @staticmethod
    def replace_uid(text: str, old_uid_list: list[int], new_uid: int) -> str:
        """
        repleace UID
        - `0000 0001 0000 0000` hex is `4294967296` decimal, so it have to be more than 10 digits, 
          so [0-9]{10,}.
        - The UID format in the GUID is '0x01234567(uid) | 01234567(unknown yet)', but after proto
          parsing, it becomes an integer, so it needs to be converted back for replacement.
        """
        decimal_pattern = re.compile(r'guid.*?:.*?([0-9]{10,})')

        def hex_replace(match: re.Match[str]):
            full_match = match.group(0)
            guid_decimal = int(match.group(1))

            current_uid = (guid_decimal >> 32) & 0xFFFFFFFF  # use high 32bits as UID
            if current_uid in old_uid_list:
                # replace with new UID
                new_guid_decimal = (new_uid << 32) | (guid_decimal & 0xFFFFFFFF)
                return full_match.replace(match.group(1), str(new_guid_decimal))
            else:
                return full_match

        return decimal_pattern.sub(hex_replace, text)

    def decode_procdure(self, blob_bytes_file: str, decode_msg: str, proto_file: str):
        if (not blob_bytes_file.startswith('5A4C4942')):
            # raise RuntimeError('Not start with `5A4C4942`(ZLIB in utf-8) as expect. ')
            print('Not start with `5A4C4942`("ZLIB" in utf-8) as expect. ')
            # return

        zlib_comp_bytes = Decoder.hex_str2bytes(blob_bytes_file)

        protoc_bin_bytes = Decoder.decompress_file(zlib_comp_bytes)

        decode_raw_str = ProtocDecoder.protoc_decode_raw(protoc_bin_bytes,
                                                         '../Mess_no_git/decoded_raw.txt')
        decoded_with_proto_str = ProtocDecoder.protoc_decode(protoc_bin_bytes,
                                                             '../Mess_no_git/decoded_with_proto.txt',
                                                             decode_msg, proto_file)

        decoded_with_proto_replaced_str = Decoder.replace_uid(decoded_with_proto_str,
                                                              _OLD_UID_INTS, _NEW_UID_INTS)
        # BUG: why two '\r\r'?
        open('../Mess_no_git/replaced_uid.txt', 'w').write(decoded_with_proto_replaced_str)

        protoc_octal_to_utf8_bytes(decode_raw_str, '../Mess_no_git/decoded_naive.txt')

        re_encoded_protoc_bin_bytes = ProtocDecoder.protoc_encode(decoded_with_proto_replaced_str,
                                                                  '../Mess_no_git/re_encoded.bin',
                                                                  decode_msg, proto_file)

        zlib_re_compress_bytes = Decoder.compress_file(re_encoded_protoc_bin_bytes)

        blob_hex_str = Decoder.bytes2hex_str(zlib_re_compress_bytes)

        open("../Mess_no_git/changed_blob.txt", 'w', encoding='utf8').write(blob_hex_str)

        return blob_hex_str


def main():
    decoder = Decoder()
    # modify here
    global _OLD_UID_INTS
    global _NEW_UID_INTS

    _OLD_UID_INTS = [142857142]
    _NEW_UID_INTS = 285714285

    # User data
    player_data_blob = decoder.dbh.get_user_bin_data(_NEW_UID_INTS, 'bin_data')
    # open(f"../Mess/blob-bak-{time.time()}.txt", 'w', encoding='utf8').write(blob_bytes_file)

    blob_hex_str = decoder.decode_procdure(player_data_blob, 'proto.PlayerDataBin', 'bin.server.proto')

    # Uncomment below if you already backup.
    # decoder.dbh.set_user_bin_data(_NEW_UID_STR, 'bin_data', blob_hex_str)

    """
    # block data dosen't contain any thing with uid, it seems be. So no need
    # to do that, but you can just have a lock of those proto dumps. 
    
    # block data
    bytes_from_db = []
    bytes_from_db.extend(decoder.dbh.get_user_block_data(_NEW_UID_STR, 'bin_data'))
    for blob_bytes in bytes_from_db:
        decoder.decode_procdure(blob_bytes, 'proto.BlockBin', 'bin.block.proto')
    """


if __name__ == "__main__":
    main()
