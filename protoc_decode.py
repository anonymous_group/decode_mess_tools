import subprocess

_PROTO_ROOT_PATH = 'F:/WorkTable/Repos/HK4E_Note/hex_blob_analysis/gameserver'
_PROTO_PATH = 'F:/WorkTable/Repos/HK4E_Note/hex_blob_analysis/gameserver/server_only'

_PROTOC_EXEC = 'F:/WorkTable/Repos/HK4E_Note/hex_blob_analysis/protoc.exe'


class ProtocDecoder:
    @staticmethod
    def protoc_decode_raw(protoc_bin_bytes: bytes, output_fp: str = ''):
        command = [_PROTOC_EXEC, '--decode_raw']
        try:
            result = subprocess.run(command, input=protoc_bin_bytes, text=False, capture_output=True,
                                    check=True, cwd=_PROTO_PATH)
            if result.stderr:
                print("Errors during decoding:", result.stderr.decode())

            if (output_fp != ''):
                with open(output_fp, 'wb') as f:
                    f.write(result.stdout)

            return result.stdout.decode('utf8')
        except subprocess.CalledProcessError as e:
            print(f"Command execution failed with error: {e}")

    @staticmethod
    def protoc_decode(protoc_bin_bytes: bytes, output_fp: str = '',
                      decode_msg: str = 'proto.PlayerDataBin',
                      proto_file: str = 'bin.server.proto') -> str:
        command = [_PROTOC_EXEC, f'--decode={decode_msg}',
                   '-I', _PROTO_ROOT_PATH,
                   '-I', _PROTO_PATH,
                   f'{proto_file}']
        try:
            result = subprocess.run(command, input=protoc_bin_bytes, text=False, capture_output=True,
                                    check=True, cwd=_PROTO_PATH)
            if result.stderr:
                print("Errors during decoding:", result.stderr.decode())

            if (output_fp != ''):
                with open(output_fp, 'wb') as f:
                    f.write(result.stdout)

            return result.stdout.decode('utf8')
        except subprocess.CalledProcessError as e:
            print(f"Command execution failed with error: {e}")

    @staticmethod
    def protoc_encode(decoded_text_str: str, output_fp: str = '',
                      decode_msg: str = 'proto.PlayerDataBin',
                      proto_file: str = 'bin.server.proto') -> bytes:
        command = [_PROTOC_EXEC, f'--encode={decode_msg}',
                   '-I', _PROTO_ROOT_PATH,
                   '-I', _PROTO_PATH,
                   f'{proto_file}']
        try:
            result = subprocess.run(command, input=decoded_text_str.encode('utf8'), text=False,
                                    capture_output=True, check=True, cwd=_PROTO_PATH)
            if result.stderr:
                print("Errors during decoding:", result.stderr.decode())

            if (output_fp != ''):
                with open(output_fp, 'wb') as f:
                    f.write(result.stdout)

            return result.stdout
        except subprocess.CalledProcessError as e:
            print(f"Command execution failed with error: {e}")


if __name__ == "__main__":
    # protoc_decode()
    pass
