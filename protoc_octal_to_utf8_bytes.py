import re
import os; os.chdir(os.path.dirname(os.path.abspath(__file__)))

# raw_data = "some string and \\350\\215\\247"  # \350\215\247 in utf8 is `荧`

class protoc_oct2bytes:
    @staticmethod
    def hex2int(text: str) -> str:
        def replace_hex(match):
            ret = ''
            hex_str = match.group(0)
            if len(hex_str) == 10:
                return f"{match.group(0)}({str(int(hex_str, 16))})"
            elif len(hex_str) == 18:
                return f"{hex_str[:10]}({str(int(hex_str[:10], 16))}){hex_str[10:]}({str(int(hex_str[10:], 16))})"
            else:
                raise Exception('Emmmm? ')

        pattern = r'0x[0-9a-fA-F]+'
        return re.sub(pattern, replace_hex, text)

    # Function to convert octal escape sequences to bytes
    @staticmethod
    def convert_octal_to_bytes(text: str) -> bytes:
        text = protoc_oct2bytes.hex2int(text)
        
        # Regular expression to find octal sequences
        octal_pattern = re.compile(r'\\[0-3][0-7][0-7]')
        byte_data = b''

        # Iterator to find all octal sequences
        last_end = 0
        for match in octal_pattern.finditer(text):
            # Get the start and end of the current octal sequence
            start, end = match.span()
            # Add preceding regular text to byte_data
            byte_data += text[last_end:start].encode('utf-8')
            # Convert octal sequence to byte and add to byte_data
            byte_data += bytes([int(text[start+1:end], 8)])
            last_end = end

        # Add any remaining text after the last octal sequence
        byte_data += text[last_end:].encode('utf-8')

        return byte_data

def protoc_octal_to_utf8_bytes(decode_raw_str: str, output_fp: str = ''):
    # Convert octal sequence to byte
    byte_data = protoc_oct2bytes.convert_octal_to_bytes(decode_raw_str)

    if (output_fp != ''):
        # Save the converted data to a file
        with open(output_fp, 'wb') as file:
            file.write(byte_data)

if __name__ == "__main__":
    # protoc_oct2bytes.protoc_octal_to_utf8_bytes('../decoded_raw.txt', '../decoded_naive.txt')
    pass
